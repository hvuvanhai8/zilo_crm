// import Home from "../pages/Home"
// import NotFound from "../pages/NotFound"
import LayoutDefault from "../layouts/default"
import LayoutLogin from "../layouts/login"
const Home = () => import(/* webpackChunkName: "home" */ "../pages/Home")
const NotFound = () => import(/* webpackChunkName: "not-found" */ "../pages/NotFound")
const ClassifiedCreate = () => import(/* webpackChunkName: "classified-create" */ "../pages/Classified/Create/index")
const Login = () => import(/* webpackChunkName: "login" */ "../pages/Login/Login")
const ListClassified = () => import(/* webpackChunkName: "list-classifield" */ "../pages/Classified/ListClassifield/ListClassifield")
const UpdateClassified = () => import(/* webpackChunkName: "update-classifield" */ "../pages/Classified/Update/index");
const SyncLogin = () => import(/* webpackChunkName: "sync-login" */ "../pages/Login/SyncLogin");
export default {
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
            meta: { requiresAuth: true , layout:LayoutDefault}
        },
        {
            path: '/dang-nhap',
            name: 'Login',
            component: Login,
            meta: { checkAuth: true,layout:LayoutLogin }
        },
        {
            path: '/classifieds',
            name: 'ListClassified',
            component: ListClassified,
            meta: { requiresAuth: true , layout:LayoutDefault}
        },
        {
            path: '/classifieds/create',
            name: 'CreateClassified',
            component: ClassifiedCreate,
            meta: { requiresAuth: true , layout:LayoutDefault}
        },
        {
            path: '/sync-login',
            name: 'SyncLogin',
            component: SyncLogin,
            meta: { layout:LayoutLogin}
        },
        {
            path: '/classifieds/:id',
            name: 'UpdateClassified',
            component: UpdateClassified,
            meta: { requiresAuth: true , layout:LayoutDefault}
        },
        {
            path: '*',
            name: 'NotFound',
            component: NotFound,
            meta: { requiresAuth: true,layout:LayoutLogin }
        },
    ]
}