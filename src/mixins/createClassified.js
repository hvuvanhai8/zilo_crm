export default {
    data(){
        return{
            news: {
                title: {
                    value: ''
                    , valid: false
                    , message: 'nhập tiêu đề'
                },
                category: {
                    id: 0
                    , label: ''
                    , valid: false
                    , message: 'chọn danh mục'
                },
                project: {
                    id: 0,
                    label: '',
                    valid: true,
                    message: 'chọn dự án'
                },
                attributes: {
                    value: [],
                    label: '',
                    valid: false,
                    message: 'Chọn thuộc tính'
                },
                description: {
                    value: '',
                    label: '',
                    valid: false,
                    message: 'Nhập mô tả'
                },
                list_price:{
                    value: 0,
                    label: '',
                    valid: false,
                    message: 'Nhập giá'
                },
                list_acreage:{
                    value: 0,
                    label: '',
                    valid: false,
                    message: 'Nhập diện tích'
                },
                list_badroom:{
                    value: 0,
                    label: '',
                    valid: true,
                    message: 'Nhập phòng ngủ'
                },
                list_toilet:{
                    value: 0,
                    label: '',
                    valid: true,
                    message: 'Nhập phòng tắm'
                },
                feature:{
                    value: {},
                    label: '',
                    valid: true,
                    message: 'Nhập thông tin nổi bật'
                }
            },
            contact: {
                phone: {
                    value: ''
                    , valid: false
                    , message: 'Nhập số điện thoại'
                },
                email: {
                    value: ''
                    , valid: true
                    , message: 'Nhập email'
                },
                contact_name: {
                    value: ''
                    , valid: false
                    , message: 'Nhập tên'
                }
            },
            address: {
                address: {
                    value: ''
                    , valid: false
                    , message: "Nhập địa chỉ"
                },
                cit_id: {
                    id: 0
                    , valid: false
                    , message: 'Chọn tỉnh/ thành phố'
                },
                dis_id: {
                    id: 0
                    , valid: false
                    , message: 'Chọn quận / huyện'
                },
                ward_id: {
                    id: 0
                    , valid: true
                    , message: 'Chọn xã/thi trấn'
                },
                street_id: {
                    id: 0
                    , valid: true
                    , message: 'Chọn đường/phố'
                },
                geo_location: {
                    value: {lat: 21.018367, lng: 105.823765}
                    , valid: true
                    , message: 'Đánh dấu bản đồ'
                }
            },
            media: {
                media: {
                    value: []
                    , valid: false
                    , message: 'Đánh dấu bản đồ'
                }
            }
        }
    },
    computed:{
        access_token:function(){
            return this.$store.getters.ACCESS_TOKEN;
        }
    },
    methods:{
        scrollTop(){
          if(this.$route.name=='CreateClassified'){
              window.scrollTopHead()
          }
        },
        checkNews() {
            var check_form = true;
            var message = [];
            if(!this.news.title.valid){
                check_form = false;
                message.push('Chưa nhập tiêu đề');
            }
            if(!this.news.category.valid){
                check_form = false;
                message.push('Chưa chọn chọn danh mục');
            }
            if(!this.news.attributes.valid){
                check_form = false;
                message.push('Chưa chọn thuộc tính');
            }
            if(!this.news.description.valid){
                check_form = false;
                message.push('Chưa nhập mô tả');
            }
            if(!this.news.list_acreage.valid){
                check_form = false;
                message.push('Chưa nhập diện tích');
            }
            if(!this.news.list_price.valid){
                check_form = false;
                message.push('Chưa nhập giá');
            }

            if(!check_form){
                var html_error = this.renderHtmlError(message);
                Swal.fire({
                    title:"Kiểm tra thông tin",
                    html:html_error,
                    icon:'warning'
                });
                return false;
            }
            this.scrollTop();
            return true;
        },
        checkAddress() {
            var check_form = true;
            var message = [];
            if(!this.address.cit_id.valid){
                check_form = false;
                message.push('Chưa chọn tỉnh/thành phố');
            }
            if(!this.address.dis_id.valid){
                check_form = false;
                message.push('Chưa chọn quận/huyện');
            }
            if(!check_form){
                var html_error = this.renderHtmlError(message);
                Swal.fire({
                    title:"Kiểm tra thông tin",
                    html:html_error,
                    icon:'warning'
                });
                return false;
            }
            this.scrollTop();
            return true;
        },
        checkContact() {
            var check_form = true;
            var message = [];
            if(!this.contact.contact_name.valid){
                check_form = false;
                message.push('Chưa nhập tên liên hệ');
            }
            if(!this.contact.phone.valid){
                check_form = false;
                message.push('Chưa nhập số liên hệ');
            }
            if(!check_form){
                var html_error = this.renderHtmlError(message);
                Swal.fire({
                    title:"Kiểm tra thông tin",
                    html:html_error,
                    icon:'warning'
                });
                return false;
            }
            this.scrollTop();
            return true;
        },
        checkMedia(submit=true) {
            var check_form = true;
            var message = [];
            if(!this.media.media.valid){
                check_form = false;
                message.push('Vui lòng chọn ảnh hoặc video');
            }
            if(!check_form){
                var html_error = this.renderHtmlError(message);
                Swal.fire({
                    title:"Kiểm tra thông tin",
                    html:html_error,
                    icon:'warning'
                });
                return false;
            }
            if(submit) this.submitData();
            this.scrollTop();
            return true;
        },
        renderHtmlError(message){
            var html_error = '<ul style="list-style: none;color:red">';
            message.forEach(item=>{
                html_error += ` <li>${item}</li>`;
            });
            html_error +='</ul>';
            return html_error;
        }
    }
}