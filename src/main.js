import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import Router from 'vue-router'
import StoreInit from './store'
import RouterInit from "./router"
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {searchKeywordObject,clearAccent} from "./helper/function"
import {ApiGet,ApiPut,ApiPost,ApiDelete} from "./helper/api"
// import Swal from 'sweetalert2/dist/sweetalert2.js'
import Cookies from "js-cookie"
window.Cookies = Cookies;
window.document.getElementById('page-loading').style.display = 'none';
// import 'sweetalert2/src/sweetalert2.scss'
// window.Swal = Swal;

Vue.component('font-awesome-icon', FontAwesomeIcon);
require('./assets/scss/index.scss');

Vue.use(Vuex);
Vue.use(Router);
window.Vue = Vue;
window.searchKeywordObject = searchKeywordObject;
window.clearAccent = clearAccent;
window.ApiGet = ApiGet;
window.ApiPost = ApiPost;
window.ApiPut = ApiPut;
window.ApiDelete = ApiDelete;

Vue.config.productionTip = false;

const store = new Vuex.Store(StoreInit);
const router = new Router(RouterInit);
let check_auth = false;

router.beforeEach(async (to, from, next) => {
    if (to.matched.some(  record => record.meta.requiresAuth)) {
        console.log('check auth');
        if(!check_auth){
            console.log('start check auth');
            var access_token = store.getters.ACCESS_TOKEN;
            var user_id = store.getters.USER_ID;
            if(access_token&&user_id){
                await ApiGet(`v2/users/${user_id}`,{access_token: access_token}).then(res=>{
                    if(res.data.active==1){
                        store.commit('SET_USER_INFO',res.data);
                        check_auth = true;
                        // next();
                    }else{
                        store.commit('SET_USER_INFO',null);
                        store.commit('SET_ACCESS_TOKEN',null);
                        store.commit('SET_USER_ID',null);
                        Cookies.remove('access_token');
                        Cookies.remove('user_id');
                        Cookies.remove('_ga');
                        Swal.fire({title:`Tài khoản ${res.data.id} đã bị khóa!`,text:'Vui lòng liên hệ với quản trị viên!',icon:'error'});
                        next({ path: '/dang-nhap'});
                        return;
                    }
                }).catch(e=>{
                    console.log('error:',e);
                    Cookies.remove('access_token');
                    Cookies.remove('user_id');
                    Cookies.remove('_ga');
                    Swal.fire({title:`Tài khoản không xác định!`,text:'Vui lòng đăng nhập lại!',icon:'error'});
                    setTimeout(()=>{
                        next({ path: '/dang-nhap'});
                    },500);
                    return;
                });
            }else{
                next({ path: '/dang-nhap'})
                return;
            }
        }
        // next();
    } else if(to.matched.some(record => record.meta.checkAuth)){
        var access_token = store.getters.ACCESS_TOKEN;
        var user_id = store.getters.USER_ID;
        if(access_token&&user_id){
            next( {path: '/'});
            return;
        }
    }
    to.matched.some( async record => {
        console.log('record.components:',record.components)
        if(typeof record.components.default == "function"){
            await record.components.default().then(async component=>{
                if( typeof component.default.asyncData =="function") {
                    await component.default.asyncData({store:store,router:router,route:to});
                }
            })
        }else{
            if(typeof record.components.default.asyncData == "function"){
                await record.components.default.asyncData({store:store,router:router,route:to})
            }
        }

        next();
    });
})

// router.beforeEach((to, from, next) => {
//     // next()
// })

window.addEventListener('online', () => {
    store.commit('SET_INTERNET_CONNECT',true)
});

window.addEventListener('offline', () => {
    store.commit('SET_INTERNET_CONNECT',false)
});

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');