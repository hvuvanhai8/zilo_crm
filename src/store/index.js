import Home from "./modules/Home"
import isMobile from 'is-mobile'
const is_mobile = isMobile();
import Cookies from "js-cookie"

export default {
    state: {
        access_token: Cookies.get('access_token') ? Cookies.get('access_token') : null,
        user_id: Cookies.get('user_id') ? parseInt(Cookies.get('user_id')) : null,
        user_info: null,
        categories: null,
        attributes: [],
        is_mobile: is_mobile,
        open_sidebar: is_mobile === true ? false : true,
        cit_locations: [

            {
                "id": 2,
                "lat": 10.5215836,
                "lng": 105.1258955,
                "name": "An Giang"
            },
            {
                "id": 3,
                "lat": 10.5417397,
                "lng": 107.2429976,
                "name": "Bà Rịa - Vũng Tàu"
            },
            {
                "id": 4,
                "lat": 21.3014947,
                "lng": 106.6291304,
                "name": "Bắc Giang"
            },
            {
                "id": 5,
                "lat": 0,
                "lng": 0,
                "name": "Bắc Kạn"
            },
            {
                "id": 6,
                "lat": 9.251555500000002,
                "lng": 105.5136472,
                "name": "Bạc Liêu"
            },
            {
                "id": 7,
                "lat": 21.121444,
                "lng": 106.1110501,
                "name": "Bắc Ninh"
            },
            {
                "id": 8,
                "lat": 10.1081553,
                "lng": 106.4405872,
                "name": "Bến Tre"
            },
            {
                "id": 9,
                "lat": 14.1665324,
                "lng": 108.902683,
                "name": "Bình Định"
            },
            {
                "id": 10,
                "lat": 11.3254024,
                "lng": 106.477017,
                "name": "Bình Dương"
            },
            {
                "id": 11,
                "lat": 11.7511894,
                "lng": 106.7234639,
                "name": "Bình Phước"
            },
            {
                "id": 12,
                "lat": 11.0903703,
                "lng": 108.0720781,
                "name": "Bình Thuận"
            },
            {
                "id": 13,
                "lat": 9.1526728,
                "lng": 105.1960795,
                "name": "Cà mau"
            },
            {
                "id": 14,
                "lat": 10.0451618,
                "lng": 105.7468535,
                "name": "Cần Thơ"
            },
            {
                "id": 15,
                "lat": 0,
                "lng": 0,
                "name": "Cao Bằng"
            },
            {
                "id": 16,
                "lat": 16.0544068,
                "lng": 108.2021667,
                "name": "Đà Nẵng"
            },
            {
                "id": 17,
                "lat": 12.7100116,
                "lng": 108.2377519,
                "name": "Đắk Lắk"
            },
            {
                "id": 18,
                "lat": 12.2646476,
                "lng": 107.609806,
                "name": "Đắk Nông"
            },
            {
                "id": 19,
                "lat": 0,
                "lng": 0,
                "name": "Điện Biên"
            },
            {
                "id": 20,
                "lat": 11.0686305,
                "lng": 107.1675976,
                "name": "Đồng Nai"
            },
            {
                "id": 21,
                "lat": 10.4937989,
                "lng": 105.6881788,
                "name": "Đồng Tháp"
            },
            {
                "id": 22,
                "lat": 13.8078943,
                "lng": 108.109375,
                "name": "Gia Lai"
            },
            {
                "id": 23,
                "lat": 0,
                "lng": 0,
                "name": "Hà Giang"
            },
            {
                "id": 24,
                "lat": 20.5835196,
                "lng": 105.92299,
                "name": "Hà Nam"
            },
            {
                "id": 25,
                "lat": 21.0277644,
                "lng": 105.8341598,
                "name": "Hà Nội"
            },
            {
                "id": 26,
                "lat": 18.2943776,
                "lng": 105.6745247,
                "name": "Hà Tĩnh"
            },
            {
                "id": 27,
                "lat": 20.9373413,
                "lng": 106.3145542,
                "name": "Hải Dương"
            },
            {
                "id": 28,
                "lat": 20.8449115,
                "lng": 106.6880841,
                "name": "Hải Phòng"
            },
            {
                "id": 29,
                "lat": 9.757897999999999,
                "lng": 105.6412527,
                "name": "Hậu Giang"
            },
            {
                "id": 30,
                "lat": 10.8230989,
                "lng": 106.6296638,
                "name": "Hồ Chí Minh"
            },
            {
                "id": 31,
                "lat": 20.6861265,
                "lng": 105.3131185,
                "name": "Hòa Bình"
            },
            {
                "id": 32,
                "lat": 20.8525711,
                "lng": 106.0169971,
                "name": "Hưng Yên"
            },
            {
                "id": 33,
                "lat": 12.2585098,
                "lng": 109.0526076,
                "name": "Khánh Hòa"
            },
            {
                "id": 34,
                "lat": 9.8249587,
                "lng": 105.1258955,
                "name": "Kiên Giang"
            },
            {
                "id": 35,
                "lat": 14.3497403,
                "lng": 108.0004606,
                "name": "Kon Tum"
            },
            {
                "id": 36,
                "lat": 0,
                "lng": 0,
                "name": "Lai Châu"
            },
            {
                "id": 37,
                "lat": 11.5752791,
                "lng": 108.1428669,
                "name": "Lâm Đồng"
            },
            {
                "id": 38,
                "lat": 21.853708,
                "lng": 106.761519,
                "name": "Lạng Sơn"
            },
            {
                "id": 39,
                "lat": 22.3380865,
                "lng": 104.1487055,
                "name": "Lào Cai"
            },
            {
                "id": 40,
                "lat": 10.695572,
                "lng": 106.2431205,
                "name": "Long An"
            },
            {
                "id": 41,
                "lat": 20.4388225,
                "lng": 106.1621053,
                "name": "Nam Định"
            },
            {
                "id": 42,
                "lat": 19.2342489,
                "lng": 104.9200365,
                "name": "Nghệ An"
            },
            {
                "id": 43,
                "lat": 20.2506149,
                "lng": 105.9744536,
                "name": "Ninh Bình"
            },
            {
                "id": 44,
                "lat": 11.6738767,
                "lng": 108.8629572,
                "name": "Ninh Thuận"
            },
            {
                "id": 45,
                "lat": 21.4219791,
                "lng": 105.2296992,
                "name": "Phú Thọ"
            },
            {
                "id": 46,
                "lat": 13.0881861,
                "lng": 109.0928764,
                "name": "Phú Yên"
            },
            {
                "id": 47,
                "lat": 17.6102715,
                "lng": 106.3487474,
                "name": "Quảng Bình"
            },
            {
                "id": 48,
                "lat": 15.5393538,
                "lng": 108.019102,
                "name": "Quảng Nam"
            },
            {
                "id": 49,
                "lat": 15.0759838,
                "lng": 108.7125791,
                "name": "Quảng Ngãi"
            },
            {
                "id": 50,
                "lat": 21.006382,
                "lng": 107.2925144,
                "name": "Quảng Ninh"
            },
            {
                "id": 51,
                "lat": 16.7943472,
                "lng": 106.963409,
                "name": "Quảng trị"
            },
            {
                "id": 52,
                "lat": 9.602521,
                "lng": 105.9739049,
                "name": "Sóc Trăng"
            },
            {
                "id": 53,
                "lat": 21.1022284,
                "lng": 103.7289167,
                "name": "Sơn La"
            },
            {
                "id": 54,
                "lat": 11.3351554,
                "lng": 106.1098854,
                "name": "Tây Ninh"
            },
            {
                "id": 55,
                "lat": 20.5386936,
                "lng": 106.3934777,
                "name": "Thái Bình"
            },
            {
                "id": 56,
                "lat": 21.5671559,
                "lng": 105.8252038,
                "name": "Thái Nguyên"
            },
            {
                "id": 57,
                "lat": 19.806692,
                "lng": 105.7851816,
                "name": "Thanh Hóa"
            },
            {
                "id": 58,
                "lat": 16.467397,
                "lng": 107.5905326,
                "name": "Thừa Thiên Huế"
            },
            {
                "id": 59,
                "lat": 10.4493324,
                "lng": 106.3420504,
                "name": "Tiền Giang"
            },
            {
                "id": 60,
                "lat": 9.812740999999999,
                "lng": 106.2992912,
                "name": "Trà Vinh"
            },
            {
                "id": 61,
                "lat": 0,
                "lng": 0,
                "name": "Tuyên Quang"
            },
            {
                "id": 62,
                "lat": 10.0861281,
                "lng": 106.0169971,
                "name": "Vĩnh Long"
            },
            {
                "id": 63,
                "lat": 21.3608805,
                "lng": 105.5474373,
                "name": "Vĩnh Phúc"
            },
            {
                "id": 64,
                "lat": 21.6837923,
                "lng": 104.4551361,
                "name": "Yên Bái"
            }
        ],
        cit_address: [
            {
                "id": 38399,
                "name": "An Giang",
                "lat": 10.5215836,
                "lng": 105.1258955,
                "geometry": "{\"location\":{\"lat\":10.5215836,\"lng\":105.1258955},\"viewport\":{\"northeast\":{\"lat\":10.960993,\"lng\":105.5753289},\"southwest\":{\"lat\":10.1832719,\"lng\":104.775528}}}"
            },
            {
                "id": 25237,
                "name": "Bà Rịa Vũng Tàu",
                "lat": 10.5417397,
                "lng": 107.2429976,
                "geometry": "{\"location\":{\"lat\":10.5417397,\"lng\":107.2429976},\"viewport\":{\"northeast\":{\"lat\":10.804375,\"lng\":107.5823451},\"southwest\":{\"lat\":8.630173,\"lng\":106.53672}}}"
            },
            {
                "id": 37615,
                "name": "Bắc Giang",
                "lat": 21.3014947,
                "lng": 106.6291304,
                "geometry": "{\"location\":{\"lat\":21.3014947,\"lng\":106.6291304},\"viewport\":{\"northeast\":{\"lat\":21.6262909,\"lng\":107.033365},\"southwest\":{\"lat\":21.122229,\"lng\":105.8803639}}}"
            },
            {
                "id": 49116,
                "name": "Bắc Kạn",
                "lat": 0,
                "lng": 0,
                "geometry": null
            },
            {
                "id": 47452,
                "name": "Bạc Liêu",
                "lat": 9.251555500000002,
                "lng": 105.5136472,
                "geometry": "{\"location\":{\"lat\":9.251555500000002,\"lng\":105.5136472},\"viewport\":{\"northeast\":{\"lat\":9.637054,\"lng\":105.859674},\"southwest\":{\"lat\":9.015005000000002,\"lng\":105.2322039}}}"
            },
            {
                "id": 30402,
                "name": "Bắc Ninh",
                "lat": 21.121444,
                "lng": 106.1110501,
                "geometry": "{\"location\":{\"lat\":21.121444,\"lng\":106.1110501},\"viewport\":{\"northeast\":{\"lat\":21.2639539,\"lng\":106.310018},\"southwest\":{\"lat\":20.969379,\"lng\":105.8889821}}}"
            },
            {
                "id": 41842,
                "name": "Bến Tre",
                "lat": 10.1081553,
                "lng": 106.4405872,
                "geometry": "{\"location\":{\"lat\":10.1081553,\"lng\":106.4405872},\"viewport\":{\"northeast\":{\"lat\":10.3397221,\"lng\":106.8149809},\"southwest\":{\"lat\":9.790454,\"lng\":106.0277731}}}"
            },
            {
                "id": 16327,
                "name": "Bình Dương",
                "lat": 11.3254024,
                "lng": 106.477017,
                "geometry": "{\"location\":{\"lat\":11.3254024,\"lng\":106.477017},\"viewport\":{\"northeast\":{\"lat\":11.5013561,\"lng\":106.966831},\"southwest\":{\"lat\":10.86301,\"lng\":106.335151}}}"
            },
            {
                "id": 35069,
                "name": "Bình Phước",
                "lat": 11.7511894,
                "lng": 106.7234639,
                "geometry": "{\"location\":{\"lat\":11.7511894,\"lng\":106.7234639},\"viewport\":{\"northeast\":{\"lat\":12.2978089,\"lng\":107.4269861},\"southwest\":{\"lat\":11.301133,\"lng\":106.4108569}}}"
            },
            {
                "id": 27862,
                "name": "Bình Thuận  ",
                "lat": 11.0903703,
                "lng": 108.0720781,
                "geometry": "{\"location\":{\"lat\":11.0903703,\"lng\":108.0720781},\"viewport\":{\"northeast\":{\"lat\":11.5553041,\"lng\":109.014595},\"southwest\":{\"lat\":10.47316,\"lng\":107.3942791}}}"
            },
            {
                "id": 35914,
                "name": "Bình Định",
                "lat": 14.1665324,
                "lng": 108.902683,
                "geometry": "{\"location\":{\"lat\":14.1665324,\"lng\":108.902683},\"viewport\":{\"northeast\":{\"lat\":14.703603,\"lng\":109.361996},\"southwest\":{\"lat\":13.5030321,\"lng\":108.597326}}}"
            },
            {
                "id": 54578,
                "name": "BĐS Mỹ, Úc",
                "lat": 0,
                "lng": 0,
                "geometry": null
            },
            {
                "id": 42323,
                "name": "Cà Mau",
                "lat": 9.1526728,
                "lng": 105.1960795,
                "geometry": "{\"location\":{\"lat\":9.1526728,\"lng\":105.1960795},\"viewport\":{\"northeast\":{\"lat\":9.268125399999999,\"lng\":105.280094},\"southwest\":{\"lat\":9.0826037,\"lng\":105.0942493}}}"
            },
            {
                "id": 27085,
                "name": "Cần Thơ",
                "lat": 10.0451618,
                "lng": 105.7468535,
                "geometry": "{\"location\":{\"lat\":10.0451618,\"lng\":105.7468535},\"viewport\":{\"northeast\":{\"lat\":10.074671,\"lng\":105.7980825},\"southwest\":{\"lat\":9.993702899999999,\"lng\":105.7170582}}}"
            },
            {
                "id": 49274,
                "name": "Cao Bằng",
                "lat": 0,
                "lng": 0,
                "geometry": null
            },
            {
                "id": 34534,
                "name": "Gia Lai",
                "lat": 13.8078943,
                "lng": 108.109375,
                "geometry": "{\"location\":{\"lat\":13.8078943,\"lng\":108.109375},\"viewport\":{\"northeast\":{\"lat\":14.6030659,\"lng\":108.872598},\"southwest\":{\"lat\":12.996112,\"lng\":107.450169}}}"
            },
            {
                "id": 48876,
                "name": "Hà Giang",
                "lat": 0,
                "lng": 0,
                "geometry": null
            },
            {
                "id": 44083,
                "name": "Hà Nam",
                "lat": 20.5835196,
                "lng": 105.92299,
                "geometry": "{\"location\":{\"lat\":20.5835196,\"lng\":105.92299},\"viewport\":{\"northeast\":{\"lat\":20.70496,\"lng\":106.183059},\"southwest\":{\"lat\":20.3626391,\"lng\":105.769326}}}"
            },
            {
                "id": 9278,
                "name": "Hà Nội",
                "lat": 21.0277644,
                "lng": 105.8341598,
                "geometry": "{\"location\":{\"lat\":21.0277644,\"lng\":105.8341598},\"viewport\":{\"northeast\":{\"lat\":21.0503801,\"lng\":105.8764459},\"southwest\":{\"lat\":20.9950991,\"lng\":105.7974815}}}"
            },
            {
                "id": 44351,
                "name": "Hà Tĩnh",
                "lat": 18.2943776,
                "lng": 105.6745247,
                "geometry": "{\"location\":{\"lat\":18.2943776,\"lng\":105.6745247},\"viewport\":{\"northeast\":{\"lat\":18.806923,\"lng\":106.50454},\"southwest\":{\"lat\":17.9136489,\"lng\":105.103354}}}"
            },
            {
                "id": 33881,
                "name": "Hải Dương",
                "lat": 20.9373413,
                "lng": 106.3145542,
                "geometry": "{\"location\":{\"lat\":20.9373413,\"lng\":106.3145542},\"viewport\":{\"northeast\":{\"lat\":20.9878068,\"lng\":106.3910866},\"southwest\":{\"lat\":20.8938575,\"lng\":106.2577702}}}"
            },
            {
                "id": 21944,
                "name": "Hải Phòng",
                "lat": 20.8449115,
                "lng": 106.6880841,
                "geometry": "{\"location\":{\"lat\":20.8449115,\"lng\":106.6880841},\"viewport\":{\"northeast\":{\"lat\":20.8792627,\"lng\":106.759901},\"southwest\":{\"lat\":20.814211,\"lng\":106.6375924}}}"
            },
            {
                "id": 46921,
                "name": "Hậu Giang",
                "lat": 9.757897999999999,
                "lng": 105.6412527,
                "geometry": "{\"location\":{\"lat\":9.757897999999999,\"lng\":105.6412527},\"viewport\":{\"northeast\":{\"lat\":9.995069899999999,\"lng\":105.896325},\"southwest\":{\"lat\":9.5820669,\"lng\":105.327878}}}"
            },
            {
                "id": 1,
                "name": "Hồ Chí Minh",
                "lat": 10.8230989,
                "lng": 106.6296638,
                "geometry": "{\"location\":{\"lat\":10.8230989,\"lng\":106.6296638},\"viewport\":{\"northeast\":{\"lat\":11.1602136,\"lng\":107.0265769},\"southwest\":{\"lat\":10.3493704,\"lng\":106.3638784}}}"
            },
            {
                "id": 38078,
                "name": "Hòa Bình",
                "lat": 20.6861265,
                "lng": 105.3131185,
                "geometry": "{\"location\":{\"lat\":20.6861265,\"lng\":105.3131185},\"viewport\":{\"northeast\":{\"lat\":21.112825,\"lng\":105.857624},\"southwest\":{\"lat\":20.30537,\"lng\":104.835105}}}"
            },
            {
                "id": 35561,
                "name": "Hưng Yên",
                "lat": 20.8525711,
                "lng": 106.0169971,
                "geometry": "{\"location\":{\"lat\":20.8525711,\"lng\":106.0169971},\"viewport\":{\"northeast\":{\"lat\":21.005412,\"lng\":106.266852},\"southwest\":{\"lat\":20.614824,\"lng\":105.8946679}}}"
            },
            {
                "id": 20641,
                "name": "Khánh Hòa",
                "lat": 12.2585098,
                "lng": 109.0526076,
                "geometry": "{\"location\":{\"lat\":12.2585098,\"lng\":109.0526076},\"viewport\":{\"northeast\":{\"lat\":12.868734,\"lng\":109.469278},\"southwest\":{\"lat\":11.768696,\"lng\":108.669095}}}"
            },
            {
                "id": 29872,
                "name": "Kiên Giang",
                "lat": 9.8249587,
                "lng": 105.1258955,
                "geometry": "{\"location\":{\"lat\":9.8249587,\"lng\":105.1258955},\"viewport\":{\"northeast\":{\"lat\":10.540138,\"lng\":105.5390449},\"southwest\":{\"lat\":9.2545629,\"lng\":103.4583964}}}"
            },
            {
                "id": 45663,
                "name": "Kon Tum",
                "lat": 14.3497403,
                "lng": 108.0004606,
                "geometry": "{\"location\":{\"lat\":14.3497403,\"lng\":108.0004606},\"viewport\":{\"northeast\":{\"lat\":14.4549609,\"lng\":108.088045},\"southwest\":{\"lat\":14.2307742,\"lng\":107.8508949}}}"
            },
            {
                "id": 48342,
                "name": "Lai Châu",
                "lat": 0,
                "lng": 0,
                "geometry": null
            },
            {
                "id": 28348,
                "name": "Lâm Đồng",
                "lat": 11.5752791,
                "lng": 108.1428669,
                "geometry": "{\"location\":{\"lat\":11.5752791,\"lng\":108.1428669},\"viewport\":{\"northeast\":{\"lat\":12.315675,\"lng\":108.7285549},\"southwest\":{\"lat\":11.214882,\"lng\":107.2671111}}}"
            },
            {
                "id": 48502,
                "name": "Lạng Sơn",
                "lat": 21.853708,
                "lng": 106.761519,
                "geometry": "{\"location\":{\"lat\":21.853708,\"lng\":106.761519},\"viewport\":{\"northeast\":{\"lat\":21.9287145,\"lng\":106.7982674},\"southwest\":{\"lat\":21.782416,\"lng\":106.6957426}}}"
            },
            {
                "id": 40422,
                "name": "Lào Cai",
                "lat": 22.3380865,
                "lng": 104.1487055,
                "geometry": "{\"location\":{\"lat\":22.3380865,\"lng\":104.1487055},\"viewport\":{\"northeast\":{\"lat\":22.844679,\"lng\":104.628431},\"southwest\":{\"lat\":21.8753179,\"lng\":103.5306449}}}"
            },
            {
                "id": 23036,
                "name": "Long An",
                "lat": 10.695572,
                "lng": 106.2431205,
                "geometry": "{\"location\":{\"lat\":10.695572,\"lng\":106.2431205},\"viewport\":{\"northeast\":{\"lat\":11.0293569,\"lng\":106.746779},\"southwest\":{\"lat\":10.3940401,\"lng\":105.5037139}}}"
            },
            {
                "id": 40867,
                "name": "Nam Định",
                "lat": 20.4388225,
                "lng": 106.1621053,
                "geometry": "{\"location\":{\"lat\":20.4388225,\"lng\":106.1621053},\"viewport\":{\"northeast\":{\"lat\":20.459585,\"lng\":106.2161637},\"southwest\":{\"lat\":20.3759291,\"lng\":106.11763}}}"
            },
            {
                "id": 32982,
                "name": "Nghệ An",
                "lat": 19.2342489,
                "lng": 104.9200365,
                "geometry": "{\"location\":{\"lat\":19.2342489,\"lng\":104.9200365},\"viewport\":{\"northeast\":{\"lat\":19.995705,\"lng\":105.8063991},\"southwest\":{\"lat\":18.5528989,\"lng\":103.874551}}}"
            },
            {
                "id": 42810,
                "name": "Ninh Bình",
                "lat": 20.2506149,
                "lng": 105.9744536,
                "geometry": "{\"location\":{\"lat\":20.2506149,\"lng\":105.9744536},\"viewport\":{\"northeast\":{\"lat\":20.2902152,\"lng\":106.0238172},\"southwest\":{\"lat\":20.2000237,\"lng\":105.9270859}}}"
            },
            {
                "id": 43589,
                "name": "Ninh Thuận",
                "lat": 11.6738767,
                "lng": 108.8629572,
                "geometry": "{\"location\":{\"lat\":11.6738767,\"lng\":108.8629572},\"viewport\":{\"northeast\":{\"lat\":12.169476,\"lng\":109.2333109},\"southwest\":{\"lat\":11.3066479,\"lng\":108.550922}}}"
            },
            {
                "id": 43158,
                "name": "Phú Thọ",
                "lat": 21.4219791,
                "lng": 105.2296992,
                "geometry": "{\"location\":{\"lat\":21.4219791,\"lng\":105.2296992},\"viewport\":{\"northeast\":{\"lat\":21.466031,\"lng\":105.283317},\"southwest\":{\"lat\":21.365241,\"lng\":105.175588}}}"
            },
            {
                "id": 43800,
                "name": "Phú Yên",
                "lat": 13.0881861,
                "lng": 109.0928764,
                "geometry": "{\"location\":{\"lat\":13.0881861,\"lng\":109.0928764},\"viewport\":{\"northeast\":{\"lat\":13.6959149,\"lng\":109.4587549},\"southwest\":{\"lat\":12.7066029,\"lng\":108.6715489}}}"
            },
            {
                "id": 45945,
                "name": "Quảng Bình",
                "lat": 17.6102715,
                "lng": 106.3487474,
                "geometry": "{\"location\":{\"lat\":17.6102715,\"lng\":106.3487474},\"viewport\":{\"northeast\":{\"lat\":18.0897489,\"lng\":106.9934901},\"southwest\":{\"lat\":16.920561,\"lng\":105.611175}}}"
            },
            {
                "id": 24445,
                "name": "Quảng Nam",
                "lat": 15.5393538,
                "lng": 108.019102,
                "geometry": "{\"location\":{\"lat\":15.5393538,\"lng\":108.019102},\"viewport\":{\"northeast\":{\"lat\":16.0667921,\"lng\":108.738184},\"southwest\":{\"lat\":14.951644,\"lng\":107.2105251}}}"
            },
            {
                "id": 41429,
                "name": "Quảng Ngãi",
                "lat": 15.0759838,
                "lng": 108.7125791,
                "geometry": "{\"location\":{\"lat\":15.0759838,\"lng\":108.7125791},\"viewport\":{\"northeast\":{\"lat\":15.4602149,\"lng\":109.083617},\"southwest\":{\"lat\":14.531423,\"lng\":108.234846}}}"
            },
            {
                "id": 30963,
                "name": "Quảng Ninh",
                "lat": 21.006382,
                "lng": 107.2925144,
                "geometry": "{\"location\":{\"lat\":21.006382,\"lng\":107.2925144},\"viewport\":{\"northeast\":{\"lat\":21.665348,\"lng\":108.108803},\"southwest\":{\"lat\":20.637977,\"lng\":106.4385801}}}"
            },
            {
                "id": 46313,
                "name": "Quảng Trị",
                "lat": 16.7943472,
                "lng": 106.963409,
                "geometry": "{\"location\":{\"lat\":16.7943472,\"lng\":106.963409},\"viewport\":{\"northeast\":{\"lat\":17.167919,\"lng\":107.389016},\"southwest\":{\"lat\":16.302481,\"lng\":106.5158501}}}"
            },
            {
                "id": 45188,
                "name": "Sóc Trăng",
                "lat": 9.602521,
                "lng": 105.9739049,
                "geometry": "{\"location\":{\"lat\":9.602521,\"lng\":105.9739049},\"viewport\":{\"northeast\":{\"lat\":9.6597732,\"lng\":106.0268426},\"southwest\":{\"lat\":9.5596392,\"lng\":105.928781}}}"
            },
            {
                "id": 47139,
                "name": "Sơn La",
                "lat": 21.1022284,
                "lng": 103.7289167,
                "geometry": "{\"location\":{\"lat\":21.1022284,\"lng\":103.7289167},\"viewport\":{\"northeast\":{\"lat\":22.0308979,\"lng\":105.025034},\"southwest\":{\"lat\":20.5728078,\"lng\":103.212191}}}"
            },
            {
                "id": 39636,
                "name": "Tây Ninh",
                "lat": 11.3351554,
                "lng": 106.1098854,
                "geometry": "{\"location\":{\"lat\":11.3351554,\"lng\":106.1098854},\"viewport\":{\"northeast\":{\"lat\":11.4384371,\"lng\":106.1880131},\"southwest\":{\"lat\":11.293289,\"lng\":106.0712941}}}"
            },
            {
                "id": 37153,
                "name": "Thái Bình",
                "lat": 20.5386936,
                "lng": 106.3934777,
                "geometry": "{\"location\":{\"lat\":20.5386936,\"lng\":106.3934777},\"viewport\":{\"northeast\":{\"lat\":20.729132,\"lng\":106.6309441},\"southwest\":{\"lat\":20.274861,\"lng\":106.109459}}}"
            },
            {
                "id": 40052,
                "name": "Thái Nguyên",
                "lat": 21.5671559,
                "lng": 105.8252038,
                "geometry": "{\"location\":{\"lat\":21.5671559,\"lng\":105.8252038},\"viewport\":{\"northeast\":{\"lat\":21.6484935,\"lng\":105.9041691},\"southwest\":{\"lat\":21.5062617,\"lng\":105.7028103}}}"
            },
            {
                "id": 31642,
                "name": "Thanh Hóa",
                "lat": 19.806692,
                "lng": 105.7851816,
                "geometry": "{\"location\":{\"lat\":19.806692,\"lng\":105.7851816},\"viewport\":{\"northeast\":{\"lat\":19.8845083,\"lng\":105.8629489},\"southwest\":{\"lat\":19.7329164,\"lng\":105.7191611}}}"
            },
            {
                "id": 29041,
                "name": "Thừa Thiên Huế",
                "lat": 16.467397,
                "lng": 107.5905326,
                "geometry": "{\"location\":{\"lat\":16.467397,\"lng\":107.5905326},\"viewport\":{\"northeast\":{\"lat\":16.742823,\"lng\":108.194138},\"southwest\":{\"lat\":15.9948179,\"lng\":107.0136741}}}"
            },
            {
                "id": 36508,
                "name": "Tiền Giang",
                "lat": 10.4493324,
                "lng": 106.3420504,
                "geometry": "{\"location\":{\"lat\":10.4493324,\"lng\":106.3420504},\"viewport\":{\"northeast\":{\"lat\":10.586974,\"lng\":106.801602},\"southwest\":{\"lat\":10.189435,\"lng\":105.8195049}}}"
            },
            {
                "id": 46665,
                "name": "Trà Vinh",
                "lat": 9.812740999999999,
                "lng": 106.2992912,
                "geometry": "{\"location\":{\"lat\":9.812740999999999,\"lng\":106.2992912},\"viewport\":{\"northeast\":{\"lat\":10.0815499,\"lng\":106.5789169},\"southwest\":{\"lat\":9.529002,\"lng\":105.953061}}}"
            },
            {
                "id": 47952,
                "name": "Tuyên Quang",
                "lat": 0,
                "lng": 0,
                "geometry": null
            },
            {
                "id": 42584,
                "name": "Vĩnh Long",
                "lat": 10.0861281,
                "lng": 106.0169971,
                "geometry": "{\"location\":{\"lat\":10.0861281,\"lng\":106.0169971},\"viewport\":{\"northeast\":{\"lat\":10.3320851,\"lng\":106.2894779},\"southwest\":{\"lat\":9.882404,\"lng\":105.682897}}}"
            },
            {
                "id": 39262,
                "name": "Vĩnh Phúc",
                "lat": 21.3608805,
                "lng": 105.5474373,
                "geometry": "{\"location\":{\"lat\":21.3608805,\"lng\":105.5474373},\"viewport\":{\"northeast\":{\"lat\":21.5739379,\"lng\":105.7943231},\"southwest\":{\"lat\":21.1520719,\"lng\":105.322191}}}"
            },
            {
                "id": 47627,
                "name": "Yên Bái",
                "lat": 21.6837923,
                "lng": 104.4551361,
                "geometry": "{\"location\":{\"lat\":21.6837923,\"lng\":104.4551361},\"viewport\":{\"northeast\":{\"lat\":22.291709,\"lng\":105.1001579},\"southwest\":{\"lat\":21.3259681,\"lng\":103.88577}}}"
            },
            {
                "id": 13326,
                "name": "Đà Nẵng",
                "lat": 16.0544068,
                "lng": 108.2021667,
                "geometry": "{\"location\":{\"lat\":16.0544068,\"lng\":108.2021667},\"viewport\":{\"northeast\":{\"lat\":16.0951345,\"lng\":108.2354165},\"southwest\":{\"lat\":15.999203,\"lng\":108.1779956}}}"
            },
            {
                "id": 26395,
                "name": "Đắk Lắk",
                "lat": 12.7100116,
                "lng": 108.2377519,
                "geometry": "{\"location\":{\"lat\":12.7100116,\"lng\":108.2377519},\"viewport\":{\"northeast\":{\"lat\":13.41631,\"lng\":108.995213},\"southwest\":{\"lat\":12.159569,\"lng\":107.4843871}}}"
            },
            {
                "id": 42160,
                "name": "Đắk Nông",
                "lat": 12.2646476,
                "lng": 107.609806,
                "geometry": "{\"location\":{\"lat\":12.2646476,\"lng\":107.609806},\"viewport\":{\"northeast\":{\"lat\":12.8121539,\"lng\":108.115649},\"southwest\":{\"lat\":11.7480449,\"lng\":107.2058339}}}"
            },
            {
                "id": 48174,
                "name": "Điện Biên",
                "lat": 0,
                "lng": 0,
                "geometry": null
            },
            {
                "id": 19463,
                "name": "Đồng Nai",
                "lat": 11.0686305,
                "lng": 107.1675976,
                "geometry": "{\"location\":{\"lat\":11.0686305,\"lng\":107.1675976},\"viewport\":{\"northeast\":{\"lat\":11.580995,\"lng\":107.577862},\"southwest\":{\"lat\":10.5227151,\"lng\":106.750922}}}"
            },
            {
                "id": 44748,
                "name": "Đồng Tháp",
                "lat": 10.4937989,
                "lng": 105.6881788,
                "geometry": "{\"location\":{\"lat\":10.4937989,\"lng\":105.6881788},\"viewport\":{\"northeast\":{\"lat\":10.974052,\"lng\":105.9422241},\"southwest\":{\"lat\":10.135533,\"lng\":105.18679}}}"
            }
        ],
        internet_connect:true
    },
    getters: {
        CATEGORIES(state) {
            return state.categories;
        },
        INTERNET_CONNECT(state){
          return state.internet_connect;
        },
        ACCESS_TOKEN(state){
          return state.access_token;
        },
        USER_ID(state){
          return state.user_id;
        },
        USER_INFO(state){
          return state.user_info;
        },
        ATTRIBUTES(state) {
            return state.attributes;
        },
        IS_MOBILE(state) {
            return state.is_mobile
        },
        OPEN_SIDEBAR(state) {
            return state.open_sidebar;
        },
        CITY_ADDRESS(state) {
            return state.cit_address.map(item => {
                if (item.geometry) {
                    item.geometry = JSON.parse(item.geometry);
                    var location = item.geometry.location;
                    item.lat = location.lat;
                    item.lng = location.lng;
                }
                return item;
            })
        },
        CITY_LOCATION(state) {
            return state.cit_locations;
        },
        CATEGORIES_CHILD(state) {
            if (state.categories != null) {
                return state.categories.filter((item) => {
                    return item.parent_id != 0;
                })
            }
            return [];
        }
    },
    mutations: {
        SET_CATEGORIES(state, payload) {
            state.categories = payload;
        },
        SET_ATTRIBUTES(state, payload) {
            state.attributes = payload
        },
        SET_OPEN_SIDEBAR(state, payload) {
            state.open_sidebar = payload;
        },
        SET_ACCESS_TOKEN(state,payload){
            state.access_token = payload;
        },
        SET_USER_ID(state, payload){
            state.user_id = payload;
        },
        SET_USER_INFO(state,payload){
            state.user_info = payload;
        },
        SET_INTERNET_CONNECT(state,payload){
            state.internet_connect = payload
        }
    },
    actions: {
        ACTION_GET_CATEGORY({state, commit}) {
            if (state.categories === null) {
                ApiGet('v2/categories', {
                    fields: 'name,id,parent_id,att_id,feature',
                    with: 'all'
                }).then(res => {
                    commit('SET_CATEGORIES', res.data);
                }).catch(error => {
                    console.error('Get category error');
                })
            }
        },
        ACTION_GET_ATTRIBUTE({state, commit}) {
            if (state.attributes.length == 0) {
                ApiGet('v2/attributes_v2', {type: 'group_attribute'}).then(res => {
                    commit('SET_ATTRIBUTES', res.data);
                }).catch(error => {
                    console.error('Get attributes error');
                })
            }
        },
        ACTION_GET_USER_INFO({state,commit}){

        }
    },
    modules: {
        Home: Home()
    }
};