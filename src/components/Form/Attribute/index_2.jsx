require('@/components/Form/Attribute/attribute.scss');
import {find} from "lodash";

export default {
    name: "Attribute",
    props:['cat_id','att_selected','required'],
    data(){
        return {
            att_selected_2:this.att_selected?this.att_selected:[],
            Valid : this.required==0?true:false
        }
    },
    watch:{
        att_selected_2:function(){
            if(this.required=='1'){
                this.att_selected_2.length<1?this.Valid = false:this.Valid = true;
            }else{
                this.Valid = true;
            }
            this.$emit('input', {
                value: this.att_selected_2,
                label: '',
                valid: this.Valid,
                message: ''
            })
        },
        cat_id:function(){
            this.att_selected_2 = [];
        }
    },
    computed: {
        attribute_show: function () {
            if(this.cat_id&&this.cat_id!==0){
                var cat_find = find(this.categories,{id:this.cat_id});
                if(cat_find){
                    return cat_find.att_id!=""?cat_find.att_id.map(item=>parseInt(item)):[];
                }
            }
            return [];
        },
        categories: function () {
            return this.$store.getters.CATEGORIES;
        },
        attributes: function () {
            return this.$store.getters.ATTRIBUTES;
        },
    },
    beforeCreate() {
        this.$store.dispatch('ACTION_GET_ATTRIBUTE');
    },
    render(h){
        const attributes = this.attributes.map((parent,key)=>{
            let status_parent_render = false;
            let parent_render = (
                <div class="attr-item-parent" key={key}>
                    <div class="attr-item-parent-name">{parent.name}</div>
                    <div class="attr-item-list">
                        {parent.attributes_child.map((item,key_child)=>{
                            let value_render = ('');
                            if(this.attribute_show.includes(item.id)){
                                if(!status_parent_render) status_parent_render = true;
                                value_render =  (
                                    <div class="attr-item-child " key={key_child}>
                                        <span class="attr-item-name">{item.name}</span>
                                        <label class="attr-item-checkbox pure-material-checkbox">
                                            <input type="checkbox" vModel={this.att_selected_2} value={item.id}/>
                                            <span></span>
                                        </label>
                                    </div>
                                );
                            }
                            return value_render;
                        })}
                    </div>
                </div>
            );
            if(status_parent_render) return parent_render;
            return ('');
        });
        return(
            <div class="list-attr">
                {attributes}
            </div>
        )
    }
}